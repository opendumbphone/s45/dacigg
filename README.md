# dacigg

Simple disassembler for Infineon C166 and C167 class microcontrollers. Forked from [hn/c166-dis](https://github.com/hn/c166-dis/)
Note: This disassembler uses the canonical assembly syntax according to the Infineon docs, not the acigg syntax.


## Usage
Requires Perl to be installed. Disassemble a binary using:
```sh
./dacigg.pl <binary> <fileOffset> <startOffset> <length>
```
Where:
- `<fileOffset>` is a hexadecimal offset indicating the beginning of the file in the initial context (e.g. an exported code block from a certain offset).
- `<startOffset>` is a hexadecimal offset indicating the beginning of the blob to be disassembled. Note that this uses the physical location from the initial context provided in `<fileOffset>`
- `<length>` is the amount of bytes to be read as a decimal number

Example: Disassemble 104 Bytes at `0x87F244` from `b1.bin`, which starts at `0x800000`:
```sh
./dacigg.pl b1.bin 0x800000 0x87F244 104
```

## Known issues
`EXTR` and derivatives do not switch labels for accessed ESFRs. Proceed with caution when reading such code and refer to the user's manual for specific addresses when in doubt.

## Contributing
Feel free to submit issues and pull requests.

## Authors and acknowledgment
Dacigg is basically just the continuation of hn's amazing work with [c166-dis](https://github.com/hn/c166-dis/).

## License
GPLv3, see heading in `dacigg.pl`
